#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXNUM  10
#define MINNUM  100
#define NUMSIZE 5

int *randnum(int size, int minnum, int maxnum) {
    srand(time(NULL));
    static int *res;
    res = (int *)malloc(sizeof(int) * size);
    for (int i = 0; i < size; i++)
        res[i] = minnum + rand() % (maxnum - minnum + 1);
    return res;
}

int **sort(int *arr, int size) {
    static int **res;
    res = (int **)malloc(size * sizeof(int *));
    for (int i = 0; i < size; i++)
        res[i] = arr + i;
    for (int i = 0; i < size - 1; i++) {
        int flag = 1, *tmp;
        for (int j = 0; j < size - i - 1; j++)
            if (*res[j] > *res[j + 1])
                tmp = res[j], res[j] = res[j + 1], res[j + 1] = tmp, flag = 0;
        if (flag) break;
    }
    return res;
}

int main() {
    int **p_sort;
    int *testarr = randnum(NUMSIZE, MINNUM, MAXNUM);

    p_sort = sort(testarr, NUMSIZE);
    for (int i = 0; i < NUMSIZE; i++) {
        if (i) printf(" ");
        printf("%d", testarr[i]);
    }
    printf("\n");
    for (int i = 0; i < NUMSIZE; i++) {
        if (i) printf(" ");
        printf("%-3d", (int)(p_sort[i] - testarr));
    }
    printf("\n");

    return 0;
}
