Function Holiday(d As String)
    Set HttpReq = CreateObject("MSXML2.ServerXMLHTTP")
    Url = "http://lanfly.vicp.io/api/holiday/info/" + d
    HttpReq.Open "get", Url, False
    HttpReq.setRequestHeader "User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"
    HttpReq.send
    ret = HttpReq.responseText

    ' 调休
    If InStr(ret, """holiday"":false") Then
        Holiday = False
    ' 国定节日
    ElseIf InStr(ret, """holiday"":true") Then
        Holiday = True
    Else
        wd = Application.WorksheetFunction.Weekday(d)
        ' 周末
        If wd = 1 Or wd = 7 Then
            Holiday = True
        Else
        ' 上班 :-(
            Holiday = False
        End If
    End If
End Function

