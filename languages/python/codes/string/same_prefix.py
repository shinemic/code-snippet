from functools import reduce
from itertools import groupby


class Words:
    def __init__(self, words):
        self.words = words

    def prefix(self):
        res = []
        for w in zip(*self.words):
            if all([c == w[0] for c in w[1:]]):
                res.append(w[0])
            else:
                break
        return ''.join(res)

    @staticmethod
    def least_common(s1, s2):
        matches = next((k, ''.join(t for t, _ in g)) for k, g
                       in groupby(zip(s1, s2), lambda x: x[0] == x[1]))
        return matches[1] if matches[0] else ''

    def prefix1(self):
        return reduce(Words.least_common, self.words)


x = Words(('flower', 'flow', 'flight'))
y = Words(('apple', 'application', 'appendix'))
z = Words(('/usr/bin', '/usr/local/bin', '/usr/'))

print(x.prefix(), x.prefix1())
print(y.prefix(), y.prefix1())
print(z.prefix(), z.prefix1())

# fl fl
# app app
# /usr/ /usr/
