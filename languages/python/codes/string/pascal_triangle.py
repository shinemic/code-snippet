def pascal_level(lastlevel):
    lastlevel.insert(0, 0)
    lastlevel.append(0)
    return list(map(sum, zip(lastlevel, lastlevel[1:])))

def printpascal_join(lines):
    pasctrig = [1]
    print(' ' * (width * (lines - 1)), "{:{}{}}".format(
          1, alignment, width), sep='')
    for line in range(2, lines + 1):
        pasctrig = pascal_level(pasctrig)
        print(' ' * (width * (lines - line)), end='')
        print((' ' * width).join(list(map(lambda x: "{:{}{}}".format(
              x, alignment, width), pasctrig))))

def printpascal_loop(lines):
    pasctrig = [1]
    align = '-' if alignment == '<' else ''
    print(' ' * width * (lines - 1), end='')
    print(f"%{align}*d" % (width, 1))
    for line in range(2, lines + 1):
        pasctrig = pascal_level(pasctrig)
        print(' ' * width * (lines - line), end='')
        for element in range(len(pasctrig)):
            if element:
                print(' ' * width, end='')
            print(f"%{align}*d" % (width, pasctrig[element]), end='')
        print()

width = 4
alignment = '<'

printpascal_join(10)
printpascal_loop(10)
