"""简单输出表格"""


def type_in(maxname=20, table_expand=4):
    user_table = []
    while True:
        username = input('用户名：')[:maxname]
        if username.lower() == 'q':
            break
        email = input('邮　箱：')
        password = input('密　码：')
        user_table.append((username, email, password))
        print()
    print()

    tabel_len = [0, 0, 0]
    title = 'Username', 'Email', 'password'
    for t in [title, *user_table]:
        for i in range(3):
            if len(t[i]) > tabel_len[i]:
                tabel_len[i] = len(t[i])
    title_underline = ['-' * l for l in tabel_len]

    formatter = (' ' * table_expand).join([f'{{:<{l}}}' for l in tabel_len])
    return '\n'.join([formatter.format(*elem) for elem in
                      [title, title_underline, *user_table]])


if __name__ == '__main__':
    table = type_in(15, 2)
    print(table)
