"""tab space 互换"""

import re
import pathlib
import fileinput
from functools import partial


def convert(root, to, glob='**/*.py', indent=4):
    p = pathlib.Path(root)
    file_list = list(map(str, p.glob(glob)))
    with fileinput.input(file_list, inplace=True) as f:
        for line in f:
            if to == 'tabs':
                print(re.sub(' ' * indent, '\t', line), end='')
            elif to == 'spaces':
                print(re.sub('\t', ' ' * indent, line), end='')

to_tabs = partial(convert, to='tabs')
to_spaces = partial(convert, to='spaces')
