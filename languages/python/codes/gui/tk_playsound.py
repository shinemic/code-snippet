import winsound
import tkinter as tk
from tkinter.font import Font
from threading import Thread
from functools import partial


def play(sound_file):
    winsound.PlaySound(sound_file, winsound.SND_FILENAME | winsound.SND_ASYNC)


def play_sound_thread(sound_file):
    t_sound = Thread(target=play, args=(sound_file, ))
    t_sound.daemon = True
    t_sound.start()


def sound_button_template(text='',
                          relief=None,
                          font=None,
                          sound_file='Sound.wav',
                          thread=play_sound_thread):
    button = tk.Button(
        root,
        relief=relief if relief else tk.GROOVE,
        text=text if text else 'audio',
        font=font if font else Font(family='Microsoft Yahei', size=8),
        command=partial(play_sound_thread, sound_file)
    )

    return button


if __name__ == "__main__":
    root = tk.Tk()
    root.minsize(width=200, height=-1)
    root.title('winsound')

    buttons = [sound_button_template(text=text, sound_file=sound) for text, sound in [
        ('1.wav', 'E:/UtilityPrograms/BaiduYun/sounds/1.wav'),
        ('2.wav', 'E:/UtilityPrograms/BaiduYun/sounds/2.wav'),
        ('3.wav', 'E:/UtilityPrograms/BaiduYun/sounds/3.wav'),
        ('4.wav', 'E:/UtilityPrograms/BaiduYun/sounds/4.wav'),
    ]]

    for btn in buttons:
        btn.pack(side=tk.LEFT, padx=15, pady=15)

    root.mainloop()
