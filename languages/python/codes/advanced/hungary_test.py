"""metaclass转化类方法名"""


class MetaClassHungary(type):
    def __new__(cls, name, bases, attrs):
        names_pool = []

        for k, v in attrs.items():
            if not k.startswith('__'):
                ks = k.split('_')
                new_k = ''.join([ks[0].lower(), *map(str.title, ks[1:])])
                names_pool.append([k, new_k, v])

        for _k, _new_k, _v in names_pool:
            del attrs[_k]
            attrs[_new_k] = _v

        attrs['__changes__'] = [(k, v) for k, v, _ in names_pool]
        return type.__new__(cls, name, bases, attrs)


class A(metaclass=MetaClassHungary):
    def method(self):
        pass

    def method_a(self):
        pass

    def methodB(self):
        pass

    def very_long_method_name(self):
        pass


a = A()
print(', '.join([x for x in dir(a) if not x.startswith('__')]))
for _old, _new in a.__changes__:
    print(f'{_old} => {_new}')
