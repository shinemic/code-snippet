**注意**

如需对上述代码利用 `PyInstaller` 打包，需要在 `.spec` 文件中加入：

```python
...
hiddenimports=[
    'pyttsx3.drivers',
    'pyttsx3.drivers.dummy',
    'pyttsx3.drivers.espeak',
    'pyttsx3.drivers.nsss',
    'pyttsx3.drivers.sapi5'
],
...
```

以及将文件 `\Lib\site-packages\PyInstaller\loader\rthooks\pyi_rth_win32comgenpy.py` 所有内容注释掉。

当前版本：

- `PyInstaller`: 3.4
- `pyttsx3`: 2.7
