"""Matlab 中的日期格式转 Pandas"""

from datetime import datetime, timedelta
from scipy.io import loadmat
import pandas as pd

mat = loadmat('data')
data = pd.DataFrame(mat['data'])
data[0] = data[0].apply(lambda x: datetime.fromordinal(
    int(x) - 366) + timedelta(days=x % 1)).dt.round('1s')
print(data)
