"""正方形分形作图 - turtle"""

import turtle

tur = turtle.Turtle()
tur.shape('blank')
tur.turtlesize(*(0.3,) * 3)
tur._delay(0)
tur.speed(0)


def draw(depth, width, start_pos):

    # 回到左上角
    tur.penup()
    tur.goto(*start_pos)

    # 横向四条线
    for i in range(4):
        tur.goto(start_pos[0], start_pos[1] - width / 3 * i)
        tur.pendown()
        tur.seth(0)
        tur.forward(width)
        tur.penup()

    # 归位
    tur.setpos(*start_pos)

    # 纵向四条线
    for i in range(4):
        tur.goto(start_pos[0] + width / 3 * i, start_pos[1])
        tur.pendown()
        tur.seth(270)
        tur.forward(width)
        tur.penup()

    # 归位
    tur.setpos(*start_pos)

    # 如果需要画更深层次图形
    if depth > 1:
        depth -= 1
        width /= 3
        for v in range(3):
            for h in range(3):
                if (h, v) != (1, 1):
                    draw(depth, width, (start_pos[0] + h * width, start_pos[1] - v * width))


# 当递归数较多时会影响显示性能，取消注释 tracer 及 update 的两行不显示绘图过程

turtle.tracer(1000, 0)
draw(5, 750, (-375, 375))
# turtle.update()
turtle.mainloop()
