"""requests_html 测试一例"""

from requests_html import HTMLSession

url = 'https://dp.nifa.org.cn/HomePage?method=getTargetOrgInfo&sorganation=91110108089606614R'

info_tb = {
    2016: '//*[@id="out-info"]',
    2017: '//*[@id="trade-log"]',
}


def p(url, page):
    r = HTMLSession().get(url)
    table = r.html.xpath(info_tb[page])[0]

    title = [x.strip() for x in
             table.lxml.xpath('//table/tr[1]/td/text()') if x.strip()]
    tbody_raw = [x.strip() for x in
                 table.lxml.xpath('//table/tr[position() > 1]/td/text()')]
    rows = len(tbody_raw) // len(title)
    date_index = (1 if page == 2016 else 2) * rows
    tbody_date = tbody_raw[:date_index]
    tbody_data = tbody_raw[date_index:]

    tbody = []
    for date, data in zip(zip(*[iter(tbody_date)] * (len(tbody_date) // rows)),
                          zip(*[iter(tbody_data)] * (len(tbody_data) // rows))):
        records = []
        records.extend(date)
        records.extend(data)
        tbody.append(records)

    print(', '.join(title[:5]))
    print()
    for r in tbody[:10]:
        print(', '.join(r[:5]))


p(url, 2016)
print('\n', '-' * 80, '\n', sep='')
p(url, 2017)
