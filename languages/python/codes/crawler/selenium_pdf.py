"""selenium 批量打印 PDF"""

import os
import json
import time
from selenium import webdriver

pages = [
    'http://reading.baicizhan.com/article/0a26bc00bffcc88bed710319b915bb1d',
    'http://reading.baicizhan.com/article/0f04a836ee01e52dfe5b1a0fb3d6a77c',
    'http://reading.baicizhan.com/article/135ceff03e80be6eb3a7943923046598',
    'http://reading.baicizhan.com/article/1d2c4f82e01a58e4bb4c70487af1e213',
    'http://reading.baicizhan.com/article/1fd808dbc733e4039060e8865dc88f07',
    'http://reading.baicizhan.com/article/238d1c1f2f2357ec5f90c99420dfb12c',
    'http://reading.baicizhan.com/article/2cdf82785a514d8a68480f3fd947b163',
    'http://reading.baicizhan.com/article/2dfc233653c0b9000c3b981e1368a33a',
    'http://reading.baicizhan.com/article/3874cd10cbbe3b426850bf2db40818b5',
    'http://reading.baicizhan.com/article/3ad378dbbed5ec157bf9f36ab5019b6b',
    'http://reading.baicizhan.com/article/4646bdac7b11e7e9d6536858f4d1be6e',
    'http://reading.baicizhan.com/article/6a5df76765ad191523faf4343c4b7dd1',
    'http://reading.baicizhan.com/article/7248582f882585456a79025ca33989f4',
    'http://reading.baicizhan.com/article/7d07b740efea3bbac7dc6bfa9f4eb038',
    'http://reading.baicizhan.com/article/7dcdb2687a3f19c2365d2d6ac39da3e6',
    'http://reading.baicizhan.com/article/84cf3c5e336ecc603951a761a6135114',
    'http://reading.baicizhan.com/article/992ac82fe68271afc348d7197943c547',
    'http://reading.baicizhan.com/article/ab190268047ad519bf7e49877144d343',
    'http://reading.baicizhan.com/article/ad1e5b006b8bbdf350cc085c3dc41ed9',
    'http://reading.baicizhan.com/article/b0e42cac9cc9d6bf66fd5b6cd36a27d0',
    'http://reading.baicizhan.com/article/b0e79070be7128f11c38c9e8fa32371b',
    'http://reading.baicizhan.com/article/d2fb209d24b44ec5fcd3c77ea4ae0a94',
    'http://reading.baicizhan.com/article/d403699ac5b56b0c72d813e31638bdfb',
    'http://reading.baicizhan.com/article/d57fc322c0e50950f26780add80274e1',
    'http://reading.baicizhan.com/article/d6fcbee1adf01738596e9c49afd128e8',
    'http://reading.baicizhan.com/article/e470d33fd49a161d77273c8a1c3b085b',
    'http://reading.baicizhan.com/article/e771f1eb539c8d9784fce89035f55f47',
    'http://reading.baicizhan.com/article/f87d04d04a9157b7c3d436ac7f0d1b13',
]

appState = {
    "recentDestinations": [
        {
            "id": "Save as PDF",
            "origin": "local"
        }
    ],
    "selectedDestinationId": "Save as PDF",
    "version": 2
}

profile = {
    'printing.print_preview_sticky_settings.appState': json.dumps(appState),
    'savefile.default_directory': r'C:\Users\Heller\Desktop\download'
}

chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option('prefs', profile)
chrome_options.add_argument('--kiosk-printing')

driver = webdriver.Chrome(chrome_options=chrome_options)
driver.implicitly_wait(10)
for p in pages:
    driver.get(p)
    title = ' '.join(driver.find_element_by_css_selector(
        '#articleTitle').text.split(' - ')[1:])
    driver.execute_script('window.print();')
    print(title)
    time.sleep(1)
    os.rename(r'C:\Users\Heller\Desktop\download\薄荷阅读MintReading.pdf',
              r'C:\Users\Heller\Desktop\download' + '\\' + title + '.pdf')
    time.sleep(1)
driver.quit()
