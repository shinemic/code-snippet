"""selenium 获取 Chrome 控制台输出"""

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import re

d = DesiredCapabilities.CHROME
d['loggingPrefs'] = {'browser': 'ALL'}
driver = webdriver.Chrome(desired_capabilities=d)

driver.get('https://www.baidu.com')

print(eval(re.findall(r'".*?"', driver.get_log('browser')[-2]['message'])[-1]))

# 同学，祝贺你喜提彩蛋~
# 或许你们还在犹豫是否加入，我会坦诚的告诉你我们超酷；
# 在这里大家都用无人车代步，AI音箱不仅播放还可以交互；
# 人工智能是发展的核心技术，做自己让未来不只领先几步；
# 在这里做自己，欢迎来到百度！
