"""便携版 Chrome + Webdriver + Selenium 配置例"""

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium import webdriver
import time
import re

chrome_options = Options()
chrome_options.binary_location = 'Chrome_62/chrome.exe'
webdriver_path = 'Utils/chromedriver-2.35.exe'

d = DesiredCapabilities.CHROME
d['loggingPrefs'] = {'browser': 'ALL'}

driver = webdriver.Chrome(
    executable_path=webdriver_path,
    chrome_options=chrome_options,
    desired_capabilities=d
)
driver.implicitly_wait(3)

driver.get('https://www.baidu.com')
time.sleep(3)
print(eval(re.findall(r'".*?"', driver.get_log('browser')[-2]['message'])[-1]))
driver.find_element_by_css_selector('map area').click()
time.sleep(1)


# 节假日才会出现
driver.switch_to_window(driver.window_handles[-1])
fest_title = driver.find_element_by_css_selector('div[class$=title]').text
fest_txt = driver.find_element_by_css_selector('div[class$=desc]').text
print(f'{fest_title}: {fest_txt}')

driver.quit()
