import pandas as pd
from itertools import groupby


def succ_invoice_number(invs):
    """将发票号码 Series 归组返回"""

    infos = []
    for _, inv in groupby(enumerate(invs), lambda x: int(x[1]) - x[0]):
        succ = [v[1] for v in inv]
        infos.append(f'{succ[0]}至{succ[-1]}' if len(succ) > 1 else f'{succ[0]}')
    return '、'.join(infos)


def parse_excel(excel_file):
    df = pd.read_excel(excel_file, dtype={'发票代码': str, '发票号码': str})
    df = df[df.是否取得发票 == '是']
    df.sort_values(by=['销方名称', '发票类型', '发票代码', '发票号码'])
    df_gb = df.groupby(by=['销方名称', '发票类型', '发票代码']).agg({
        '发票号码': succ_invoice_number,
        '金额(元)': 'sum',
        '税额(元)': 'sum',
        '价税合计(元)': 'sum',
    }).reset_index()
    df_gb['发票代码号码'] = '发票代码：' + df_gb['发票代码'] + \
        '，发票号码：' + df_gb['发票号码']
    df_result = df_gb.groupby(by=['销方名称', '发票类型']).agg({
        '发票代码号码': lambda x: '，'.join(x),
        '金额(元)': 'sum',
        '税额(元)': 'sum',
        '价税合计(元)': 'sum'
    }).reset_index()
    df_cnt = df.groupby(by=['销方名称', '发票类型'])['发票号码'].count() \
        .reset_index().rename(columns={'发票号码': '发票数量'})
    df_result = df_result.merge(df_cnt, on=['销方名称', '发票类型'])
    for _, row in df_result.iterrows():
        print('取得{销方名称}{发票类型}{发票数量}份，{发票代码号码}，'
              '金额共计{金额(元):,.2f}元，税额合计{税额(元):,.2f}元，'
              '价税合计{价税合计(元):,.2f}元。'.format_map(row))


parse_excel('./data.xlsx')
