 WITH A AS (
    SELECT 销方名称,
           发票类型,
           发票代码,
           发票号码,
           CAST(发票号码 AS NUMBER) - ROW_NUMBER() OVER (
               PARTITION BY 销方名称, 发票类型, 发票代码
               ORDER BY 发票号码) 发票号码区间,
           金额元,
           税额元,
           价税合计元
      FROM INVOICE_BOOK
     WHERE 是否取得发票 = '是'),
B AS (
    SELECT A.*,
           CASE
               WHEN COUNT(1) OVER(
                   PARTITION BY 销方名称, 发票类型, 发票代码, 发票号码区间) = 1 THEN 发票号码
               ELSE (MIN(发票号码) OVER(
                         PARTITION BY 销方名称, 发票类型, 发票代码, 发票号码区间) || '至' ||
                     MAX(发票号码) OVER(
                         PARTITION BY 销方名称, 发票类型, 发票代码, 发票号码区间))
           END AS 发票号码组
      FROM A),
C AS (
    SELECT 销方名称,
           发票类型,
           GROUP_CONCAT(发票代码号码,'，') 发票代码号码,
           SUM(金额) 金额,
           SUM(税额) 税额,
           SUM(价税合计) 价税合计
      FROM (SELECT 销方名称,
                   发票类型,
                   '发票代码：' || 发票代码 || '，' || '发票号码：' ||
                   GROUP_CONCAT(发票号码组,'、') 发票代码号码,
                   SUM(金额) 金额,
                   SUM(税额) 税额,
                   SUM(价税合计) 价税合计
              FROM (SELECT 销方名称,
                           发票类型,
                           发票代码,
                           发票号码组,
                           SUM(金额元) 金额,
                           SUM(税额元) 税额,
                           SUM(价税合计元) 价税合计
                      FROM B
                  GROUP BY 销方名称,
                           发票类型,
                           发票代码,
                           发票号码组)
          GROUP BY 销方名称,
                   发票类型,
                   发票代码)
  GROUP BY 销方名称,
           发票类型)
   SELECT C.*,
          D.发票数量
     FROM C
LEFT JOIN (   SELECT 销方名称,
                     发票类型,
                     COUNT(*) 发票数量
                FROM A
            GROUP BY 销方名称, 发票类型) D
       ON C.销方名称 = D.销方名称
      AND C.发票类型 = D.发票类型
