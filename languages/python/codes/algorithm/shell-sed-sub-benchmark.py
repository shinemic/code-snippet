"""Shell 正则替换 v.s. 调用 sed 替换 时间对比"""

import timeit
from subprocess import Popen, PIPE, STDOUT, run
import re
import io


def wrapper(func, *args, **kwargs):
    def wrapped():
        return func(*args, **kwargs)
    return wrapped


def gen_str(size=1000) -> str:
    import random
    import string
    alphabeta = string.ascii_lowercase
    return ''.join(random.choices(alphabeta, k=size))


def task1_py(s) -> None:
    return re.sub(r'a[^z]+z', '<cycle>', s)


def task2_py(s) -> None:
    p = run('sed -r "s#a[^z]+z#<cycle>#g"',
            input=s.encode('utf-8'),
            stdout=PIPE, shell=True)
    return p.stdout


s = gen_str(10**5)
times = 1000

t1 = timeit.timeit(stmt=wrapper(task1_py, s), number=times)
t2 = timeit.timeit(stmt=wrapper(task2_py, s), number=times)
print(t1)
print(t2)

# 0.6350650221575052
# 13.731775453081354
