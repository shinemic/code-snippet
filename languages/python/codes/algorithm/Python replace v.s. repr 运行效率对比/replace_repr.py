import time
import random


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%s -- %2.6f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed


@timeit
def f1(s):
    return s.replace('\r', '\\r')


@timeit
def f2(s):
    return repr(s)[1:-1]


def equal_test(s):
    '''相等性测试'''

    f1_res = f1(s)
    f2_res = f2(s)
    return f1_res == f2_res


def gen_str(n, m):
    dat_raw = [str(random.randint(1, 1e9)) for _ in range(N)]
    dat_raw.extend(['\r'] * M)
    random.shuffle(dat_raw)
    dat_str = ''.join(dat_raw)

    return dat_str


if __name__ == '__main__':
    N = 500000       # 随机数数量
    M = 10000       # '\r' 数量
    T = 10          # 相等性测试例数

    print(all([equal_test(gen_str(N, M)) for _ in range(T)]))
