"""递归作业一例"""

def recursive_reverse_up_to_n(name, n):
    assert n > 0 and isinstance(n, int), "`n` should be positive integer!"

    n = min(len(name), n)
    if n == 1:
        return name
    else:
        return recursive_reverse_up_to_n(name[1:n], n - 1) + name[0] + name[n:]


def build_repr(operations, templates):
    assert len(operations) >= 1 and len(operations) % 2 == 1 \
        and all(op in templates
                for op in operations
                if isinstance(op, str)), "NOT invalid input!"

    if len(operations) == 1:
        return operations[0]

    return templates[operations[1]].format(operations[0], build_repr(operations[2:], templates))


def unit_test(func, *args):
    print(f'{func.__name__}{tuple(args)}\n\t=> {func(*args)}')


if __name__ == "__main__":
    testcases = [
        (recursive_reverse_up_to_n, 'abcd', 6),
        (recursive_reverse_up_to_n, 'Nabi', 3),
        (recursive_reverse_up_to_n, '0123456789', 5),
        (recursive_reverse_up_to_n, '0123456789', 10),
        (build_repr, [0, '+', 1, '-', 3], {'+': '({0}, +, {1})', '-': '({0}, -, {1})'}),
        (build_repr, [0, '+', 1, '-', 3],{'+': '({0}, add, {1})', '-': '({0}, minus, {1})'}),
        (build_repr, [0, '+', 1, '-', 3],{'+': '({1}, add, {0})', '-': '({1}, minus, {0})'}),
    ]

    for arg in testcases:
        unit_test(*arg)
