"""素数生成时间 ~ O(n log n)"""

from datetime import datetime


def isprime(n):
    if n == 2:
        return True
    if not n % 2:
        return False
    else:
        for k in range(2, int(n ** 0.5) + 1):
            if n % k:
                continue
            else:
                return False
    return True


def unittest(sns):
    with open('output.txt', 'w') as o:
        for sn in sns:
            cnt = 1
            curnum = 2
            start = datetime.now()
            fmt = "%d, %.4f\n"
            while cnt < sn:
                while not isprime(curnum):
                    curnum += 1
                curnum += 1
                cnt += 1
            delta = datetime.now() - start
            if hasattr(delta, 'seconds'):
                o.write(fmt % (sn, delta.seconds + delta.microseconds / 1000000))
            else:
                o.write(fmt % (sn, delta.microseconds / 1000000))
            o.flush()


sns = (5 * x ** 2 for x in range(10, 500, 5))
unittest(sns)
