"""TwoSum 问题"""

import random


class Solution(object):
    def __init__(self, lower, target, n):
        self.lower = lower
        self.target = target
        self.n = n
        self.upper = 2 * target - lower
        self.nums = []

    def __str__(self):
        return 'lower: {}\nupper: {}\nn: {}\ntarget: {}\nnums: {}'.format(
            self.lower, self.upper, self.n, self.target, self.nums
        )

    def genData(self):
        if self.n < 2:
            self.nums = None
            return
        tabus = set()
        first = random.randint(self.lower, self.upper)
        match_first = self.target - first
        self.nums.extend([first, match_first])
        tabus.update([first, match_first])
        for _ in range(self.n - 2):
            while True:
                rv = random.randint(self.lower, self.upper)
                if not (rv in self.nums or (self.target - rv) in tabus):
                    self.nums.append(rv)
                    tabus.update([self.target - rv])
                    break
        random.shuffle(self.nums)

    def twoSum(self):
        matchPool = dict()
        for i, x in enumerate(self.nums):
            if self.target - x in matchPool:
                return [matchPool[self.target - x], i]
            else:
                matchPool[x] = i

        return None


if __name__ == "__main__":
    sol = Solution(-2, 5, 6)
    sol.genData()
    print(sol)
    print()
    print(sol.twoSum())
