n=123456789; \
seq $n > b && printf "       n: $n\nfilesize: %s\n" $(stat -c '%s' b); \
python3 -c 'import math; \
            n = int(input()); \
            print("~ python:", 2*n+(n+1)*math.floor(math.log10(n))-\
            10*(10**math.floor(math.log10(n))-1)//9)' <<< $n

# 公式: $2n+(n+1)[\log_{10}n]-\dfrac{10}{9}\left(10^{[\log_{10}n]}-1\right)$