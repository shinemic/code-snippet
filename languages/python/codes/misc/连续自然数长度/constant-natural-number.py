def cntm(m):
    ndig = int(math.log10(m)) + 1
    return (9*10**(ndig-1)*(ndig-1)-10**(ndig-1)+1)//9+(m-10**(ndig-1)+1)*ndig