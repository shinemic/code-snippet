class Solution:
    def __init__(self, homepage):
        self.cur_url = homepage
        self.back_urls = []
        self.forward_urls = []

    def visit(self, url):
        self.forward_urls.clear()
        self.back_urls.append(self.cur_url)
        self.cur_url = url
        return self.cur_url

    def back(self):
        if self.back_urls:
            self.forward_urls.append(self.cur_url)
            self.cur_url = self.back_urls.pop()
        else:
            return None
        return self.cur_url

    def forward(self):
        if self.forward_urls:
            self.back_urls.append(self.cur_url)
            self.cur_url = self.forward_urls.pop()
        else:
            return None
        return self.cur_url

    def exec(self, cmd, *args):
        router = {
            'VISIT': self.visit,
            'BACK': self.back,
            'FORWARD': self.forward,
        }
        return router[cmd](*args)

    def run(self, verbose=False):
        while True:
            req = input()
            cmd = req.split()[0]
            if cmd == 'QUIT':
                break
            ret = self.exec(*req.split())
            print(ret if ret else 'Ignored')
            if verbose:
                print('>>> ', end='')
                print(' | '.join([','.join(self.back_urls),
                                  self.cur_url, ','.join(reversed(self.forward_urls))]))


if __name__ == "__main__":
    Solution('0').run(True)
