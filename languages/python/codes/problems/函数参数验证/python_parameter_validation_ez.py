def parameter_debugger(arglist):
    '''
    Verify and correct `arglist` to meet the rules in Python

    Legal order of parameters:
        arg -> vararg -> kwarg -> varkwarg
    '''

    # define parameter structure
    args = []
    varargs = []
    kwargs = []
    varkwargs = []

    # check out the pattern of single parameter
    for i, arg in enumerate(arglist):
        if arg.startswith('**'):
            varkwargs.append((i, arg))
        elif arg.startswith('*'):
            varargs.append((i, arg))
        elif '=' in arg:
            kwargs.append((i, arg))
        else:
            args.append((i, arg))

    order_list = [
        *[x[0] for x in args],
        *[x[0] for x in varargs],
        *[x[0] for x in kwargs],
        *[x[0] for x in varkwargs],
    ]

    valid = all([
        sorted(order_list) == order_list,
        len(varargs) <= 1,
        len(varkwargs) <= 1
    ])

    del varargs[1:]
    del varkwargs[1:]

    params = tuple([
        *[x[1] for x in args],
        *[x[1] for x in varargs],
        *[x[1] for x in kwargs],
        *[x[1] for x in varkwargs],
    ])

    return (params, valid)


if __name__ == "__main__":
    testcases = [
        ('slope', '*constants', 'intercept'),
        ('slope', 'intercept', '*constants'),
        ('slope', ),
        ('*constants', ),
        ('**kwargs', ),
        ('a=1', 'b = 2', '*c'),
        ('*args', '**kwargs'),
        ('*args1', '*args2'),
        ('**kwargs1', '**kwargs2'),
        ('**f', 'e=2', 'a', 'b=1', '*c'),
    ]

    for case in testcases:
        res = parameter_debugger(case)
        if res[1]:
            print(f'correct: {case}')
        else:
            print(f'origin: {case} | after: {res[0]}')
