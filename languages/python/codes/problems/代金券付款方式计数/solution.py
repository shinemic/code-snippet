"""Just for Job."""


def ways_to_destination(distance: int,
                        coupon_types: int,
                        coupon_values: list,
                        tolls: int,
                        toll_distances: list):
    def ways(pieces: list, distance: int):
        # At the start point
        if distance == 0:
            return 1

        # Only one elem in pieces:
        if len(pieces) == 1:
            if distance % pieces[0] == 0:
                return 1
            else:
                return 0

        # The minimum piece is greater than distance, so there's no way
        if min(pieces) > distance:
            return 0

        maxc = distance // pieces[-1]
        # for c in range(1, maxc+1):
        return sum([ways(pieces[:-1][:], distance - c*pieces[-1])
                    for c in range(maxc+1)])

    res = 1
    base_loc = [0] + toll_distances[:-1]
    for (dis, dis_former) in zip(toll_distances, base_loc):
        res *= ways(coupon_values[:], dis - dis_former)

    return res


distance = 100
couponTypes = 4
couponValues = [5, 10]
tolls = 3
tollDistances = [11]

print(ways_to_destination(distance, couponTypes,
                          couponValues, tolls, tollDistances))
