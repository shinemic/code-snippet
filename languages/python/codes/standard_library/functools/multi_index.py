"""Python 多级访问列表一例"""

from functools import reduce
from random import randint


class MyList(list):
    def __getitem__(self, i):
        if isinstance(i, int):
            return super().__getitem__(i)
        elif isinstance(i, tuple):
            return reduce(lambda x, y: list.__getitem__(x, y), i, self)
        elif isinstance(i, list):
            return self.__getitem__(tuple(i))


a = MyList(['last'])
for i in range(10):
    a = MyList([randint(-10, 10), a])

print(a)
print()
pos = (-1, ) * 11
print(a[pos])
