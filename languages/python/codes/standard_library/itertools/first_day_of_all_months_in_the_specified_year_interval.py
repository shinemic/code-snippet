"""itertools 应用 -- 指定年份间隔中所有月份第一天"""


def gen_first_day(start, end, date_fmt='%Y%m%d'):
    import itertools
    from datetime import date

    date_pieces = itertools.product(range(start, end + 1), range(1, 13), (1,))
    return [date(*d).strftime(date_fmt) for d in date_pieces]


if __name__ == '__main__':
    print(gen_first_day(2010, 2012))

# '20100101', '20100201', '20100301', '20100401', '20100501', '20100601', ...
