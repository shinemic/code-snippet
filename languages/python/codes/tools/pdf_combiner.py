"""合并多个PDF文件"""

# pip install pypdf2

from PyPDF2 import PdfFileMerger
import sys
import os.path as op


def merger(input_paths):
    pdf_merger = PdfFileMerger()
    file_handles = []

    for path in input_paths:
        pdf_merger.append(path)

    output_path = '{}/{}.pdf'.format(
        op.dirname(sys.argv[0]),
        '+'.join([op.splitext(op.basename(p))[0] for p in input_paths]))
    with open(output_path, 'wb') as fileobj:
        pdf_merger.write(fileobj)


if __name__ == '__main__':
    merger(sys.argv[1:])
