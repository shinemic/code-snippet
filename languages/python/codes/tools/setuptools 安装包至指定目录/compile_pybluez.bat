rem 可能需要于 VS 2017的开发人员命令提示符 下进行编译

set testdir=F:\WorkingDirectory\Work\__current\test

set pybluez_repo=https://github.com/pybluez/pybluez.git
set python=E:\Miniconda\envs\app\python.exe
set prefix=%testdir%\extra
set PYTHONPATH=%prefix%\Lib\site-packages

cd /d %testdir%
git clone %pybluez_repo%
mkdir %prefix%\Lib\site-packages

cd /d %testdir%\pybluez
%python% setup.py install --prefix=%prefix%