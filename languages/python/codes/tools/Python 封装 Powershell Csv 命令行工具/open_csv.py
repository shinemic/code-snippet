# -*- encoding: utf-8 -*-

from __future__ import unicode_literals
import subprocess
import sys


def csv_file(file):
    cmd_fmt = 'powershell -NoExit -command "%(param)s"'
    cmd_param_fmt = "Import-Csv '%(file)s' | Out-GridView -wait; exit"
    cmd = cmd_fmt % {'param': cmd_param_fmt % {'file': file}}
    subprocess.Popen(cmd, shell=True)


def main():
    for file in sys.argv[1:]:
        csv_file(file)


if __name__ == '__main__':
    main()
