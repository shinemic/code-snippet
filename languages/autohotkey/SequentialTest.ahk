﻿; AHK连续判断

#NoEnv
SetBatchLines, -1

FT := Functools
OutputDebug % FT.Sequential(1, FT.LT, 3, FT.GE, 2)
OutputDebug % FT.Sequential(1, FT.LT, 3, FT.LT, 5, FT.LT, 7, FT.LE, 9)
OutputDebug % FT.Sequential(1, FT.LT, 3, FT.GT, 5)

; 1
; 1
; 0

class Functools {
    LT(a, b) {
        Return a < b
    }

    GT(a, b) {
        Return a > b
    }

    LE(a, b) {
        Return a <= b
    }

    GE(a, b) {
        Return a >= b
    }

    ;~ 操作符连续比较
    ;~ @param args: 操作数及操作符，例如：1, LT, 3, GE, 2
    Sequential(args*) {
        lope := args.RemoveAt(1)
        While (args.Count()) {
            opr := args.RemoveAt(1)
            rope := args.RemoveAt(1)
            If (%opr%(%this%, lope, rope)) {
                lope := rope
            } Else {
                Return False
            }
        }
        Return True
    }
}
