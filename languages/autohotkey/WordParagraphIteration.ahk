﻿; Word VBA 遍历并修改段落（AHK）

#NoEnv
SetWorkingDir, %A_ScriptDir%
SetBatchLines, -1

global wordApp := ComObjActive("Word.Application")
global doc := wordApp.ActiveDocument
global content := doc.Content
AddUnderLines()
Sleep, 200
RemoveUnderLines()


AddUnderLines() {
    matchedPos := []
    matchedCnt := 0
    insertText := "--------------------------------`n"
    Loop % content.Paragraphs.Count {
        If ((rng := content.Paragraphs(A_Index).Range).Text ~= "^Step\d") {
            matchedPos.Push(rng.End + StrLen(insertText) * matchedCnt++)
        }
    }

    For _, pos In matchedPos
        doc.Range(pos, pos).InsertAfter(insertText)

}

RemoveUnderLines() {
    matchedPos := []
    sumMatchedLen := 0
    Loop % content.Paragraphs.Count
        If ((rng := content.Paragraphs(A_Index).Range).Text ~= "^-+") {
            matchedPos.Push([rng.Start - sumMatchedLen, rng.Start + StrLen(rng.Text) - sumMatchedLen])
            sumMatchedLen += StrLen(rng.Text)
        }

    For _, pos In matchedPos
        doc.Range(pos[1], pos[2]).Text := ""
}
