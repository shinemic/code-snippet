﻿; Idea 运行自动聚焦光标至命令窗

$+F10::
    If (WinActive("ahk_exe pycharm64.exe") Or WinActive("ahk_exe pycharm.exe")
        Or WinActive("ahk_exe idea64.exe") Or WinActive("ahk_exe idea.exe")) {
        Send, {Esc}
        Send, {ShiftDown}{F10}{ShiftUp}
        Sleep, 500
        Send, {AltDown}4{AltUp}
    }
Return
